
'use strict';

var feedManager = require('./lib/feed_manager'),
    requestWrapper = require('helpers/request_wrapper'),
    url = require('url'),
    Q = require('q'),
    cheerio = require('cheerio');

var feedTools = require('./lib/feed_tools');

module.exports = {
    addFeed: function (id, url) {
        if (!id || !url) {
            throw 'FeedController::addFeed::Invalid arguments! id: ' + id + ', url: ' + url;
        } else if (feedManager.getJob(id)) {
            throw 'FeedController::addFeed::Duplicated id, id:' + id;
        }

        return feedManager.createFeed(id, {url: url});
    },
    removeFeed: function (id) {
        if (!feedManager.getJob(id)) {
            throw 'FeedController::removeFeed::id not found! id: ' + id;
        }
        return feedManager.removeJob(id);
    },
    getFeed: function (id) {
        var feed = feedManager.getJob(id);
        return feed;
    },
    getAllFeeds: function () {
        return feedManager.getCurrentJobs();
    },
    invokeFeed: function (id) {
        var feed = feedManager.getJob(id);
        if (!feed) {
            throw 'FeedController::invokeFeed::Feed not found! id: ' + id;
        }
        return feed.invoke();
    },
    getFeedMetaFromUrl: function (baseUrl) {
        return requestWrapper.request(baseUrl).then(function (result) {
            var title, icon, feedUrl, doc = cheerio(result.contents),
                id = url.parse(baseUrl).hostname.replace(/\..+/, '');
            title = doc.find('title').text();
            doc.find('link').each(function () {
                var $this = cheerio(this),
                    href = $this.attr('href');

                if (/favicon/.test(href)) {
                    icon = url.resolve(baseUrl, href);
                }

                if (/atom/i.test($this.attr('title'))) {
                    feedUrl = url.resolve(baseUrl, href);
                } else if (!feedUrl && /rss/i.test($this.attr('title'))) {
                    feedUrl = url.resolve(baseUrl, href);
                }
            });

            return {
                name: title,
                icon: icon,
                url: feedUrl,
                id: id
            };
        });
    },
    validateFeedUrl: function (feedUrl) {
        return feedTools.getFeedContents(feedUrl).then(function (results) {
            return feedTools.parseFeedContents(results.contents).then(function (results) {
                if (results.articles && results.articles.length) {
                    return true;
                } else {
                    return Q.reject(new Error('No articles!'));
                }
            });
        });

    }
};
