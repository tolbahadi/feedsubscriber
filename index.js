'use strict';


var feedManager = require('./lib/feed_manager');

module.exports = {
    router: require('./routes'),
    feedManager: feedManager,
    controller: require('./controller')
};
