'use strict';

var _ = require('underscore');

function ArticleFilter() {
    this.cachedArticles = [];
    this.cachedArticlesByLink = {};
    this.cachedArticlesByTitle = {};
    this.cachedArticlesByPublishDate = {};
}

ArticleFilter.prototype.filter = function(feedArticles) {
    var articles = [],
        articlesByLink = {},
        articlesByTitle = {},
        articlesByPublishDate = {};

    // remove duplicates
    feedArticles = _.filter(feedArticles, function(article) {
        var articleToCache;
        if (!articlesByLink[article.link] && !articlesByTitle[article.title] && !articlesByPublishDate[article.publishDate]) {
            articleToCache = {
                link: article.link,
                publishDate: article.publishDate,
                lastUpdateDate: article.lastUpdateDate,
                title: article.title
            };
            articlesByTitle[article.title] = articleToCache;
            articlesByLink[article.link] = articleToCache;
            articlesByPublishDate[article.publishDate] = articleToCache;
            articles.push(articleToCache);
            return true;
        }
        return false;
    });

    var newArticles = [];
    var updatedArticles = [];

    // get new articles
    _.each(articles, function(article, index) {
        var duplicate = this.cachedArticlesByLink[article.link] || this.cachedArticlesByTitle[article.title] || this.cachedArticlesByPublishDate[article.publishDate];
        if (!duplicate) {
            newArticles.push(feedArticles[index]);
        } else if (duplicate.link !== article.link || duplicate.title !== article.title || duplicate.lastUpdateDate !== article.lastUpdateDate) {
            feedArticles[index].oldLink = duplicate.link;
            updatedArticles.push(feedArticles[index]);
        }
    }, this);

    // get deleted articles
    var deletedArticles = [];
    var pendingArticles = [];

    _.each(this.cachedArticles, function(article) {
        var articleInFeed = articlesByLink[article.link] || articlesByTitle[article.title] || articlesByPublishDate[article.publishDate];
        if (!articleInFeed) {
            pendingArticles.push(article);
        } else if (pendingArticles.length) {
            deletedArticles.push.apply(deletedArticles, pendingArticles);
            pendingArticles.length = 0;
        }
    }, this);

    this.cachedArticles = articles;
    this.cachedArticlesByLink = articlesByLink;
    this.cachedArticlesByTitle = articlesByTitle;
    this.cachedArticlesByPublishDate = articlesByPublishDate;

    return {
        newArticles: newArticles,
        updatedArticles: updatedArticles,
        deletedArticles: deletedArticles
    };
};

module.exports = ArticleFilter;