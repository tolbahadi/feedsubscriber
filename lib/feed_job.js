'use strict';

var JobScheduler = require('job').JobScheduler,
    FeedTools = require('./feed_tools'),
    _ = require('underscore'),
    moment = require('moment'),
    ArticleFilter = require('./article_filter'),
    FeedRun = require('./feed_run');

var FeedProcess = JobScheduler.extend({
    feedType: 'feed_job',
    constructor: function (options) {
        JobScheduler.call(this, options);
        this.id = options.id;
        this.url = options.url;

        this.refreshInterval = options.refreshInterval || 30; // thirty minutes

        this.lastRunTime = moment().subtract('days', 1); // yesterday

        this.totalRuns = 0;
        this.totalFailedRuns = 0;

        this.articleFilter = new ArticleFilter();
    },
    toJSON: function () {
        return this;
    },
    scheduleNextRun: function () {
        var delay = this.refreshInterval;

        var hour = moment().utc().hour();
        if (hour >= 20 && hour <= 6) { // is it night time?
            delay = 120;
        }

        var nextRun = +moment().add('minutes', delay);

        // TODO: move this to job module
        if (this.isScheduled()) {
            this.cancel();
        }

        this.schedule(nextRun);
    },
    fetchFeedContents: function () {
        var self = this;
        // fetch feed contents
        return FeedTools.getFeedContents(self.url).then(function (results) {
            self.trigger('FetchedFeedContents', self, results);
            self.currentRun.feedContents = results.contents;
            self.currentRun.feedContentsDuration = results.duration;
            return results.contents;
        });
    },
    parseFeedContents: function () {
        var self = this;
        // parse feed contents into an array of articles
        return FeedTools.parseFeedContents(self.currentRun.feedContents).then(function (results) {
            self.trigger('ParsedFeedContents', self, results);

            self.currentRun.feedArticles = results.articles;

            _.each(results.articles, function (ar) {
                ar.feedId = self.id;
                ar.feedUrl = self.url;
                ar.publishDate = ar.publishedDate;
            });

            return results.articles;
        });
    },
    filterArticles: function () {
        var filterResult = this.articleFilter.filter(this.currentRun.feedArticles);

        _.extend(this.currentRun, filterResult);

        return this.currentRun;
    },
    markRunEnd: function (runResults) {
        runResults.endTime = +moment();
        runResults.duration = (runResults.endTime - runResults.startTime) / 1000;
        this.lastRunTime = moment(runResults.startTime);
    },
    run: function () {
        // jshint newcap:false
        var self = this;

        var now = +moment();

        var runResults = this.currentRun = new FeedRun({
            id: 'RUN_' + now,

            startTime: now,
            endTime: null,
            duration: null,

            url: null,

            feedContents: null,
            feedContentsDuration: null,

            feedArticles: null,

            newArticles: null,
            updateArticles: null,
            deletedArticles: null,

            failed: false
        });

        var runPromise = self.fetchFeedContents().then(function () {
            return self.parseFeedContents();
        });

        runPromise = runPromise.then(function () {
            self.filterArticles();

            self.markRunEnd(runResults);

            self.trigger('FeedUpdate', self, runResults);

            return runResults;
        }).fail(function (err) {
            self.markRunEnd(runResults);
            var message = 'RunFailed:RunDuration:' + runResults.duration + ':Error:' + (err && err.message ? err.message : err);
            console.error(message);
            runResults.failed = true;
            self.trigger('Error', self, err, runResults);
            return runResults;
        }).then(function () {
            self.scheduleNextRun();

            if (runResults.failed) {
                self.totalFailedRuns++;
            }

            self.totalRuns++;

            return runResults;
        });

        return runPromise;
    }
});

module.exports = FeedProcess;
