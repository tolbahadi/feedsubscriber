'use strict';

var FeedJob = require('./feed_job'),
    _ = require('underscore'),
    JobManager = require('job').JobManager;

var JobFeedManager = JobManager.extend({
	jobType: FeedJob,
    createFeed: function (id, options) {
        return this.createJob(id, _.extend({id: id}, options));
    }
});

module.exports = new JobFeedManager();