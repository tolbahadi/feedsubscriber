'use strict';

var _ = require('underscore');

/**
 * FeedRun class
 * An object that holds all details about a single feed run, these include:
 * - id
 * - feedId
 * - feedUrl
 * - startTime
 * - endTime
 * - duration
 * - feedContent
 * - feedContentDuration
 * - feedArticles
 * - newArticles
 * - updatedArticles
 * - logs
 * - hasError
 * - error
 */
function FeedRun (attributes) {
    _.extend(this, attributes);
}

module.exports = FeedRun;