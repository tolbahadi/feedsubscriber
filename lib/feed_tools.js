'use strict';

var url = require('url');

var feedparser = require('feedparser'),
    requestWrapper = require('helpers/request_wrapper'),
    _ = require('underscore');

// jshint newcap:false

var API = {};

_.extend(API, {
    getFeedContents: function (feedUrl) {
        var urlObj = url.parse(feedUrl);

        urlObj.query = _.extend({}, urlObj.query, { time: + new Date() });

        feedUrl = url.format(urlObj);

        return requestWrapper.request({
            uri: feedUrl
        }).then(function (results) {
            results.contents = API.fixFeedContents(results.contents);
            return results;
        });
    },
    fixFeedContents: function (contents) {
        return contents.replace('\ufeff', '');
    },
    parseFeedContents: function (contents) {
        return feedparser.parseFeedContents(contents).then(function (parsedContents) {
            return parsedContents;
        });
    }
});

module.exports = API;
