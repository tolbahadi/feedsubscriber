'use strict';

var controller = require('./controller'),
	express = require('express'),
	bodyParser = require('body-parser');

var router = module.exports = new express.Router();

router.use(bodyParser.json());

// get current feed ids
router.get('/', function (req, res) {
	res.send(controller.getAllFeeds());
});

// add feed
router.post('/', function (req, res) {
	var feedId = req.body.feedId,
		feedUrl = req.body.feedUrl;

	if (!feedId || !feedUrl) {
		res.status(500).send('Incorrect params. ' + JSON.stringify(req.body));
		return;
	} else if (controller.getFeed(feedId)) {
		res.status(500).send('Feed already exist!');
		return;
	}

	res.send(controller.addFeed(feedId, feedUrl));
});

// get feed
router.get('/:feedId', function (req, res) {
	var feed = controller.getFeed(req.params.feedId);
	if (!feed) {
		res.status(404).send('Feed not found');
	} else {
		res.send(feed);
	}
});

// remove feed
router.delete('/:feedId', function (req, res) {
	var feed = controller.getFeed(req.params.feedId);
	if (feed) {
		res.send(controller.removeFeed(req.params.feedId));
	} else {
		res.status(500).send('Feed not found');
	}
});

// invoke feed
router.get('/:feedId/invoke', function (req, res) {
	var feed = controller.getFeed(req.params.feedId);
	if (!feed) {
		res.status(500).send('Feed not found');
	} else {
		controller.invokeFeed(req.params.feedId).done(function (results) {
			res.send(results);
		}, function (err) {
			res.status(500).send(err);
			console.error(err);
		});
	}
});
