'use strict';

require('should');

var _ = require('underscore');
var ArticleFilter = require('../lib/article_filter');

describe('ArticleFilter', function () {
    beforeEach(function () {
        this.articles = _.times(4, function (index) {
            return {
                link: 'link_' + index,
                title: 'title_' + index,
                publishDate: 'publishDate_' + index,
                lastUpdateDate: 'lastUpdateDate_' + index,
            };
        });

        this.articleFilter = new ArticleFilter();
    });

    it('Fist time all are new articles', function () {
        var result = this.articleFilter.filter(this.articles);

        result.newArticles.should.eql(this.articles);
        result.updatedArticles.should.eql([]);
        result.deletedArticles.should.eql([]);
    });

    it('Return nothing when passed the same articles', function () {
        this.articleFilter.filter(this.articles);
        
        var result = this.articleFilter.filter(this.articles);

        result.newArticles.should.eql([]);
        result.updatedArticles.should.eql([]);
        result.deletedArticles.should.eql([]);
    });

    it('Adding one new articles', function () {
        this.articleFilter.filter(_.without(this.articles, this.articles[0]));
        
        var result = this.articleFilter.filter(this.articles);

        result.newArticles.should.eql([this.articles[0]]);
        result.updatedArticles.should.eql([]);
        result.deletedArticles.should.eql([]);
    });

    it('Updating one new articles', function () {
        this.articleFilter.filter(_.map(this.articles, _.clone));

        this.articles[3].title = 'new article title';
        
        var result = this.articleFilter.filter(this.articles);

        result.newArticles.should.eql([]);
        result.updatedArticles.should.eql([this.articles[3]]);
        result.deletedArticles.should.eql([]);
    });

    it('Updating one article link', function () {
        this.articleFilter.filter(_.map(this.articles, _.clone));

        this.articles[3].link = 'new article link';

        var result = this.articleFilter.filter(this.articles);

        result.newArticles.should.eql([]);
        result.updatedArticles.should.eql([this.articles[3]]);
        result.deletedArticles.should.eql([]);
    });

    it('Deleting one new articles', function () {
        this.articleFilter.filter(this.articles);

        var result = this.articleFilter.filter(_.tail(this.articles));

        result.newArticles.should.eql([]);
        result.updatedArticles.should.eql([]);
        result.deletedArticles.should.eql([this.articles[0]]);
    });

    it('Removes duplicate', function () {
        var article = this.articles[2];
        article.link = 'other link';
        var result = this.articleFilter.filter(this.articles.concat([article]));

        result.newArticles.should.eql(this.articles);
        result.updatedArticles.should.eql([]);
        result.deletedArticles.should.eql([]);
    });
});