'use strict';

var requestWrapper = require('helpers/request_wrapper'),
    feedparser = require('feedparser'),
    Q = require('q'),
    sinon = require('sinon');

var FeedJob = require('../lib/feed_job');

describe('FeedJob', function () {
    before(function () {
        sinon.stub(requestWrapper, 'request').returns(Q.resolve({contents: 'google feed contents', duration: 100}));
        sinon.stub(feedparser, 'parseFeedContents', function () {
            return Q.resolve({
                lastUpdateDate: new Date(),
                articles: [
                    {link: 'link2', title: 'Title 2', content: 'content 2'},
                    {link: 'link3', title: 'Title 3', content: 'content 3'},
                    {link: 'link4', title: 'Title 4', content: 'content 4'}
                ]
            });
        });
    });
    after(function () {
        requestWrapper.request.restore();
        feedparser.parseFeedContents.restore();
    });

    it('works', function (done) {
        var job = new FeedJob({id: 'google', url: 'google.com'});

        job.cachedLinks = ['link3'];

        job.run().done(function (results) {
            console.log(results);
            done();
        }, done);
    });
});