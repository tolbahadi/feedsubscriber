'use strict';

var should = require('should'),
    sinon = require('sinon'),
    Q = require('q');

var requestWrapper = require('helpers/request_wrapper'),
    feedparser = require('feedparser');

var FeedTools = require('../lib/feed_tools');

describe('FeedTools Module', function () {

    describe('#getFeedContents', function () {
        before(function () {
            sinon.stub(requestWrapper, 'request').returns(Q.resolve({contents: 'google feed', duration: 100}));
        });
        after(function () {
            requestWrapper.request.restore();
        });
        it('works', function (done) {
            FeedTools.getFeedContents('google').done(function (results) {
                results.should.be.ok;
                results.duration.should.eql(100);
                results.contents.should.eql('google feed');
                done();
            }, done);
        });
        it('works 2', function (done) {
            FeedTools.getFeedContents('google').done(function (results) {
                results.should.be.ok;
                results.duration.should.eql(100);
                results.contents.should.eql('google feed');
                done();
            }, done);
        });
    });

    describe('#parseFeedContents', function () {
        var parsedContents = { articles: [{summary: 'summary 1'}, {content: 'content'}], lastUpdateDate: 1 };
        before(function () {
            sinon.stub(feedparser, 'parseFeedContents').returns(Q.resolve(parsedContents));
        });
        after(function () {
            feedparser.parseFeedContents.restore();
        });
        it('works', function (done) {
            FeedTools.parseFeedContents('contents', 'google').done(function (results) {
                results.should.be.ok;
                results.lastUpdateDate.should.eql(1);
                results.articles[0].should.eql({summary: 'summary 1'});
                results.articles[1].should.eql({content: 'content'});
                done();
            }, done);
        });
    });
});